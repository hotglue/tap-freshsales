"""Stream type classes for tap-freshsales."""

from pathlib import Path
from typing import Any, Dict, Optional, Union, List, Iterable

from singer_sdk import typing as th  # JSON Schema typing helpers

from tap_freshsales.client import freshsalesStream

class FiltersStream(freshsalesStream):
    """Define Base Filter stream."""
    primary_keys = ["id"]
    records_jsonpath = "$.filters[*]"

    schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("name", th.StringType),
        th.Property("model_class_name", th.StringType),
        th.Property("user_id", th.IntegerType),
        th.Property("is_default", th.BooleanType),
        th.Property("is_public", th.BooleanType),
        th.Property("updated_at", th.DateTimeType)
    ).to_dict()

    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        """Return a context dictionary for child streams."""
        return {
            "view_id": record["id"],
        }


class ContactsViewsStream(FiltersStream):
    """Define Contacts Views stream."""
    name = "contacts_views"
    path = "contacts/filters"


class ContactsStream(freshsalesStream):
    """Define Contacts stream."""
    name = "contacts"
    path = "contacts/view/{view_id}"
    primary_keys = ["id", "updated_at", "view_id"]
    records_jsonpath = "$.contacts[*]"
    replication_key = "updated_at"
    parent_stream_type = ContactsViewsStream
    fields_url = "settings/contacts/fields"


class LeadsViewsStream(FiltersStream):
    """Define Leads Views stream."""
    name = "leads_views"
    path = "leads/filters"


class LeadsStream(freshsalesStream):
    """Define Leads stream."""
    name = "leads"
    path = "leads/view/{view_id}"
    primary_keys = ["id", "updated_at", "view_id"]
    records_jsonpath = "$.leads[*]"
    replication_key = "updated_at"
    parent_stream_type = LeadsViewsStream
    fields_url = "settings/leads/fields"
    include = ["lead_stage", "lead_reason"]


class DealsViewsStream(FiltersStream):
    """Define Deals Views stream."""
    name = "deals_views"
    path = "deals/filters"


class DealsStream(freshsalesStream):
    """Define Deals stream."""
    name = "deals"
    path = "deals/view/{view_id}"
    primary_keys = ["id", "updated_at", "view_id"]
    records_jsonpath = "$.deals[*]"
    replication_key = "updated_at"
    parent_stream_type = DealsViewsStream
    fields_url = "settings/deals/fields"
    include = ["sales_account", "contacts", "owner", "created_at"]
    static_schema = [
        th.Property("contact_ids", th.ArrayType(th.IntegerType)),
    ]


class AccountsViewsStream(FiltersStream):
    """Define Accounts Views stream."""
    name = "accounts_views"
    path = "sales_accounts/filters"


class AccountsStream(freshsalesStream):
    """Define Accounts stream."""
    name = "accounts"
    path = "sales_accounts/view/{view_id}"
    primary_keys = ["id", "updated_at", "view_id"]
    records_jsonpath = "$.sales_accounts[*]"
    replication_key = "updated_at"
    parent_stream_type = AccountsViewsStream
    fields_url = "settings/sales_accounts/fields"


class TasksStream(freshsalesStream):
    """Define Tasks stream."""
    name = "tasks"
    path = "tasks?filter={filter}"
    primary_keys = ["id"]
    records_jsonpath = "$.tasks[*]"
    partitions = [{"filter": "open"}, {"filter": "completed"}]
    schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("status", th.IntegerType),
        th.Property("title", th.StringType),
        th.Property("description", th.StringType),
        th.Property("created_at", th.DateTimeType),
        th.Property("updated_at", th.DateTimeType),
        th.Property("owner_id", th.IntegerType),
        th.Property("due_date", th.DateTimeType),
        th.Property("creater_id", th.IntegerType)
    ).to_dict()


class AppointmentsStream(freshsalesStream):
    """Define Appointments stream."""
    name = "appointments"
    path = "appointments?filter={filter}"
    primary_keys = ["id"]
    records_jsonpath = "$.appointments[*]"
    partitions = [{"filter": "past"}, {"filter": "upcoming"}]
    schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("time_zone", th.StringType),
        th.Property("title", th.StringType),
        th.Property("description", th.StringType),
        th.Property("location", th.StringType),
        th.Property("from_date", th.DateTimeType),
        th.Property("end_date", th.DateTimeType),
        th.Property("creater_id", th.IntegerType),
        th.Property("latitude", th.StringType),
        th.Property("longitude", th.StringType),
        th.Property("checkedin_at", th.DateTimeType),
        th.Property('is_allday', th.BooleanType),
        th.Property('outcome_id', th.StringType),
        th.Property('created_at', th.DateTimeType),
        th.Property('updated_at', th.DateTimeType),
        th.Property('provider', th.StringType),
        th.Property('can_checkin_checkout', th.BooleanType),
        th.Property('checkedout_latitude', th.StringType),
        th.Property('checkedout_longitude', th.StringType),
        th.Property('checkedout_location', th.StringType),
        th.Property('checkedout_at', th.DateTimeType),
        th.Property('checkedin_duration', th.StringType),
        th.Property('can_checkin', th.BooleanType),
        th.Property('conference_id', th.StringType),
        th.Property('filter', th.StringType)
    ).to_dict()
