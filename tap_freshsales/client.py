"""REST client handling, including freshsalesStream base class."""

import requests
import pendulum
import pytz
import backoff
import time
from typing import Any, Callable, Dict, Optional, Iterable
import logging

from memoization import cached

from singer_sdk import typing as th
from singer_sdk.helpers.jsonpath import extract_jsonpath
from singer_sdk.streams import RESTStream
from singer_sdk.authenticators import APIKeyAuthenticator
from backports.cached_property import cached_property
from singer_sdk.exceptions import FatalAPIError, RetriableAPIError

logging.getLogger("backoff").setLevel(logging.CRITICAL)


class freshsalesStream(RESTStream):
    """freshsales stream class."""

    records_jsonpath = "$[*]"
    next_page_token_jsonpath = "$.meta.total_pages"
    is_last_page = False
    include = None
    static_schema = None

    @property
    def url_base(self) -> str:
        """Return the API URL root, configurable via tap settings."""
        return f"https://{self.config['org_name']}.freshsales.io/api/"

    @property
    def authenticator(self) -> APIKeyAuthenticator:
        """Return a new authenticator object."""
        return APIKeyAuthenticator.create_for_stream(
            self,
            key="Authorization",
            value=f"Token token={self.config['auth_token']}",
            location="header",
        )

    @property
    def http_headers(self) -> dict:
        """Return the http headers needed."""
        headers = {}
        if "user_agent" in self.config:
            headers["User-Agent"] = self.config.get("user_agent")
        return headers

    def get_next_page_token(
        self, response: requests.Response, previous_token: Optional[Any]
    ) -> Optional[Any]:
        """Return a token for identifying next page or None if no more pages."""
        if self.is_last_page:
            self.is_last_page = False
            return None
        total_pages = next(
            extract_jsonpath(self.next_page_token_jsonpath, response.json()), None
        )
        if not total_pages:
            return None
        previous_token = previous_token or 1
        if total_pages <= previous_token:
            return None

        return previous_token + 1

    def get_url_params(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Dict[str, Any]:
        """Return a dictionary of values to be used in URL parameterization."""
        params: dict = {}
        if self.replication_key:
            params["sort"] = self.replication_key
            params["sort_type"] = "desc"
        if next_page_token:
            params["page"] = next_page_token
        if self.include:
            params["include"] = ",".join(self.include)
        return params

    def post_process(self, row: dict, context: Optional[dict]) -> dict:
        """As needed, append or transform raw data to match expected structure."""
        if row.get("custom_field"):
            row.update(row["custom_field"])
            del row["custom_field"]
        if isinstance(context, dict) and context.get("view_id"):
            row["view_id"] = context.get("view_id")
        if self.replication_key:
            self.start_date = self.get_starting_timestamp(context or {})
            self.start_date = self.start_date.astimezone(pytz.utc)
            updated = row.get(self.replication_key)
            updated = pendulum.parse(updated).astimezone(pytz.utc)
            if self.start_date >= updated:
                self.is_last_page = True
                return None
            else:
                return row
        return row

    def parse_response(self, response: requests.Response) -> Iterable[dict]:
        """Parse the response and return an iterator of result rows."""
        yield from extract_jsonpath(self.records_jsonpath, input=response.json())

    @staticmethod
    def extract_type(field):
        field_type = field.get("type")
        if field_type in ["textarea", "text", "radio"]:
            return th.StringType
        if field_type in ["dropdown"]:
            return th.CustomType({"type": ["integer", "string"]})
        if field_type in ["group_field", "multi_select_dropdown"]:
            return th.CustomType({"type": ["array", "string"]})
        if field_type in ["auto_complete"]:
            return th.CustomType({"type": ["array", "integer", "number", "string"]})
        if field_type == "number":
            return th.CustomType({"type": ["number", "string"]})
        if field_type == "date":
            return th.DateTimeType
        if field_type in ["checkbox"]:
            return th.BooleanType
        else:
            return None

    def get_fields(self):
        headers = self.http_headers
        headers.update(self.authenticator.auth_headers or {})
        response = requests.get(self.url_base + self.fields_url, headers=headers)
        if response.status_code == 429 and response.headers.get("retry-after"):
            self.logger.info(f"Hit FreshSales RateLimit. Retry after {response.headers['retry-after']} seconds...")
            time.sleep(int(response.headers['retry-after']))
        self.validate_response(response)
        return response.json()

    @cached_property
    def schema(self):
        fields = self.request_decorator(self.get_fields)()

        properties = [
            th.Property("id", th.IntegerType),
            th.Property("view_id", th.IntegerType),
            th.Property("display_name", th.StringType),
            th.Property("avatar", th.StringType),
            th.Property("email", th.StringType),
            th.Property("open_deals_amount", th.StringType),
            th.Property("won_deals_amount", th.StringType),
            th.Property("links", th.CustomType({"type": ["object", "string"]})),
            th.Property("won_deals_count", th.IntegerType),
            th.Property("open_deals_count", th.IntegerType),
            th.Property("is_deleted", th.BooleanType),
            th.Property("team_user_ids", th.CustomType({"type": ["array", "string"]})),
            th.Property(
                "subscription_status", th.CustomType({"type": ["integer", "string"]})
            ),
            th.Property("customer_fit", th.IntegerType),
        ]

        if self.static_schema:
            properties.extend(self.static_schema)

        if fields.get("errors"):
            raise Exception(fields["errors"])

        for field in fields["fields"]:
            property = th.Property(field.get("name"), self.extract_type(field))
            properties.append(property)

        return th.PropertiesList(*properties).to_dict()

    def validate_response(self, response: requests.Response) -> None:
        """Validate HTTP response."""
        if 400 <= response.status_code < 429:
            msg = (
                f"{response.status_code} Client Error: "
                f"{response.reason} for path: {self.path}"
            )
            raise FatalAPIError(msg)

        elif 429 <= response.status_code < 600:
            msg = (
                f"{response.status_code} Server Error: "
                f"{response.reason} for path: {self.path}"
            )
            raise RetriableAPIError(msg)

    def request_decorator(self, func: Callable) -> Callable:
        """Instantiate a decorator for handling request failures."""
        decorator: Callable = backoff.on_exception(
            backoff.expo,
            (
                RetriableAPIError,
                requests.exceptions.ReadTimeout,
            ),
            max_tries=10,
            factor=2,
        )(func)
        return decorator