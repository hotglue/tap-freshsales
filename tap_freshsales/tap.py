"""freshsales tap class."""

from typing import List

from singer_sdk import Tap, Stream
from singer_sdk import typing as th
from tap_freshsales.streams import (
    ContactsViewsStream,
    ContactsStream,
    LeadsViewsStream,
    LeadsStream,
    DealsViewsStream,
    DealsStream,
    AccountsViewsStream,
    AccountsStream,
    TasksStream,
    AppointmentsStream
)

STREAM_TYPES = [
    ContactsViewsStream,
    ContactsStream,
    LeadsViewsStream,
    LeadsStream,
    DealsViewsStream,
    DealsStream,
    AccountsViewsStream,
    AccountsStream,
    TasksStream,
    AppointmentsStream
]


class Tapfreshsales(Tap):
    """freshsales tap class."""
    name = "tap-freshsales"

    config_jsonschema = th.PropertiesList(
        th.Property(
            "auth_token",
            th.StringType,
            required=True,
            description="The token to authenticate against the API service"
        ),
        th.Property(
            "org_name",
            th.StringType,
            required=True,
            description="Org name used as subdomain"
        ),
        th.Property(
            "start_date",
            th.DateTimeType,
            required=False,
            default="2000-01-01"
        )
    ).to_dict()

    def discover_streams(self) -> List[Stream]:
        """Return a list of discovered streams."""
        return [stream_class(tap=self) for stream_class in STREAM_TYPES]

if __name__=="__main__":
    Tapfreshsales.cli()